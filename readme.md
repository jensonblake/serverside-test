Nathan Bradshaw - https://jsainsburyplc.github.io/serverside-test/

Please find attached a submission for the test above. 

This project uses maven, and can be compiled as you would expect from command line. 
	mvn install

I have also included a prebuilt jar; "nb.jar", which can be run with the usual command;
	java -jar nb.jar
	
The project works (at least as far as I can tell!). Given more time I would;

- Tighten up unit tests, for e.g. around Item.java.
-- I havn't added tests here as they'd be pretty standard boilerplate unit tests, and wouldn't offer massive value...
-- ...plus I've spent enough time on this exam!

- Improve ItemParser to use JSoup's 'Selector' functionality. 
-- I've never used JSoup prior to this (I wanted to try it) so I'm certain some of this code could be improved.
-- I hate the hard-coded paths to page elements and it would be nice to do something cleverer.

- This project contains TestNG and loads sample html from Resources
-- I came to the idea of using the actual website data as test-data late; with more time I think I would extend this...
-- ...and ideally at a more integration-style test as an enhancement / rewrite of AppTest.java. 

	
