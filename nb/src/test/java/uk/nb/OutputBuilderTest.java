package uk.nb;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import uk.nb.json.Item;
import uk.nb.json.OutputBuilder;

public class OutputBuilderTest {
	
	final String exampleJson = "{\n" + 
			"  \"results\": [\n" + 
			"    {\n" + 
			"      \"title\": \"Sainsbury's Strawberries 400g\",\n" + 
			"      \"kcal_per_100g\": 33,\n" + 
			"      \"unit_price\": 1.75,\n" + 
			"      \"description\": \"by Sainsbury's strawberries\"\n" + 
			"    },\n" + 
			"    {\n" + 
			"      \"title\": \"Sainsbury's Blueberries 200g\",\n" + 
			"      \"kcal_per_100g\": 45,\n" + 
			"      \"unit_price\": 1.75,\n" + 
			"      \"description\": \"by Sainsbury's blueberries\"\n" + 
			"    },\n" + 
			"    {\n" + 
			"      \"title\": \"Sainsbury's Cherry Punnet 200g\",\n" + 
			"      \"kcal_per_100g\": 52,\n" + 
			"      \"unit_price\": 1.5,\n" + 
			"      \"description\": \"Cherries\"\n" + 
			"    }" + 
			"\n" + 
			"  ],\n" + 
			"  \"total\": {\n" + 
			"    \"gross\": 5.0,\n" + 
			"    \"vat\": 1.0\n" + 
			"  }\n" + 
			"}";
			
	private final OutputBuilder objectUnderTest = new OutputBuilder();
	
	@Test
	public void testBuildBasicJson()
	{
		List<Item> items = new ArrayList<>();		
		
		Item item1 = new Item();
		item1.setTitle("Sainsbury's Strawberries 400g");
		item1.setDescription("by Sainsbury's strawberries");
		item1.setUnit_price("1.75");
		item1.setKcal_per_100g("33");
		items.add(item1);
		
		Item item2 = new Item();
		item2.setTitle("Sainsbury's Blueberries 200g");
		item2.setDescription("by Sainsbury's blueberries");
		item2.setUnit_price("1.75");
		item2.setKcal_per_100g("45");
		items.add(item2);
		
		Item item3 = new Item();
		item3.setTitle("Sainsbury's Cherry Punnet 200g");
		item3.setDescription("Cherries");
		item3.setUnit_price("1.5");
		item3.setKcal_per_100g("52");
		items.add(item3);
			
		final String json = this.objectUnderTest.buildJson(items);
		Assert.assertEquals(json, exampleJson);
	}
}
