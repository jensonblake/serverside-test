package uk.nb;

import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.jsoup.nodes.Document;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import uk.nb.io.PageFetcher;
import uk.nb.json.Item;

public class ItemParserTest {
	
	final static String VALID_ITEM_URL = "/shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html";
	
	@Mock 
	PageFetcher mockPageFetcher;
	
	@InjectMocks
	private ItemParser objectUnderTest = new ItemParser();
	
	@BeforeClass
	public void beforeClass()
	{
		MockitoAnnotations.initMocks(this);
	}
	
	@BeforeMethod
	public void beforeMethod()
	{
		Mockito.reset(mockPageFetcher);
	}
	
	@Test
	public void testParseItem() throws IOException
	{
		// Given
		Document doc = getSampleDocument();

		// When
		when(this.mockPageFetcher.fetchDocument(Mockito.anyString())).thenReturn(doc);
		Item reply = this.objectUnderTest.parseItem(VALID_ITEM_URL);
		
		// Then
		assertNotNull(reply);
		assertEquals(reply.getTitle(), "Sainsbury's Strawberries 400g");
		assertEquals(reply.getKcal_per_100g().intValue(), 33);
		assertEquals(reply.getUnit_price(), 1.75);
		assertEquals(reply.getDescription(), "by Sainsbury's strawberries");
	}
	
	
	@Test
	public void testParseItem_noNutrition() throws IOException
	{
		// Given
		Document doc = getSampleDocument_noTable();

		// When
		when(this.mockPageFetcher.fetchDocument(Mockito.anyString())).thenReturn(doc);
		
		
		Item reply = this.objectUnderTest.parseItem(VALID_ITEM_URL);
		
		// Then
		assertNotNull(reply);
		assertNull(reply.getKcal_per_100g());
		assertEquals(reply.getTitle(), "Sainsbury's Mixed Berries 300g");
		assertEquals(reply.getUnit_price(), 3.50);
		assertEquals(reply.getDescription(), "by Sainsbury's mixed berries");
	}
	
	
	private Document getSampleDocument() throws IOException
	{
		Document doc = Document.createShell(VALID_ITEM_URL);
		doc.html(loadSampleFileString("sample.html"));
		return doc;
	}
	
	
	private String loadSampleFileString(final String fileName) throws IOException
	{
		ClassLoader classLoader = new App().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());
        return new String(Files.readAllBytes(file.toPath()));
	}
	
	
	private Document getSampleDocument_noTable() throws IOException
	{
		Document doc = Document.createShell(VALID_ITEM_URL);
		doc.html(loadSampleFileString("sample_no_nutrition.html"));
		return doc;
	}
}
