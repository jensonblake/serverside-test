package uk.nb;

import java.io.IOException;
import java.net.MalformedURLException;
import static org.mockito.Mockito.*;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.collections.Lists;

import uk.nb.exception.ParsingException;
import uk.nb.io.PageFetcher;
import uk.nb.json.Item;
import uk.nb.json.OutputBuilder;

public class WebsiteParserTest {

	final static String VALID_URL = "http://www.reallycoolsite.com";
	final static String INVALID_URL = "invalid_url";
	final static String VALID_ITEM_URL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-british-strawberries-400g.html";
	final static String JSON = "{\"results\":[{\"title\":\"Sainsbury\\u0027s Strawberries 400g\",\"kcal_per_100g\":33,\"unit_price\":1.75,\"description\":\"by Sainsbury\\u0027s strawberries\"},{\"title\":\"Sainsbury\\u0027s Blueberries 200g\",\"kcal_per_100g\":45,\"unit_price\":1.75,\"description\":\"by Sainsbury\\u0027s blueberries\"},{\"title\":\"Sainsbury\\u0027s Cherry Punnet 200g\",\"kcal_per_100g\":52,\"unit_price\":1.5,\"description\":\"Cherries\"}],\"total\":{\"gross\":5.0,\"vat\":0.83}}";
	
	
	@Mock
	ItemParser mockItemParser;
	
	@Mock 
	OutputBuilder mockOutputBuilder;
	
	@Mock 
	PageFetcher mockPageFetcher;
	
	@InjectMocks
	final private WebsiteParser objectUnderTest = new WebsiteParser(); 
	
	@BeforeClass
	public void beforeClass()
	{
		MockitoAnnotations.initMocks(this);
	}
	
	@BeforeMethod
	public void beforeMethod()
	{
		Mockito.reset(mockItemParser, mockOutputBuilder, mockPageFetcher);
	}
	
	@Test
	public void testConnectAndParse_happyPath() throws ParsingException, MalformedURLException
	{
		// Given
		Document doc = mock(Document.class);
		Elements elts = new Elements();
		Element elt = new Element(Tag.valueOf("a"), "");
		elts.add(elt);
		elt.attr("href", VALID_ITEM_URL);
		Item expectedItem = new Item();

		// When
		when(this.mockPageFetcher.fetchDocument(VALID_URL)).thenReturn(doc);
		when(doc.getElementsByClass("productNameAndPromotions")).thenReturn(elts);
		when(this.mockOutputBuilder.buildJson(ArgumentMatchers.anyList())).thenReturn("");
		when(this.mockItemParser.parseItem(VALID_ITEM_URL)).thenReturn(expectedItem);
		when(this.mockOutputBuilder.buildJson(Lists.newArrayList(expectedItem))).thenReturn(JSON);
		
		final String reply = objectUnderTest.connectAndParse(VALID_URL);
		
		// Then
		Assert.assertEquals(reply, JSON);
		
		verify(doc).getElementsByClass("productNameAndPromotions");
		verify(this.mockPageFetcher, times(1)).fetchDocument(VALID_URL);
		verify(this.mockItemParser, times(1)).parseItem(VALID_ITEM_URL);
		verify(this.mockOutputBuilder, times(1)).buildJson(Lists.newArrayList(expectedItem));
	}
	
	@Test(expectedExceptions = MalformedURLException.class)
	public void testConnectAndParse_InvalidUrl_empty() throws IOException
	{
		objectUnderTest.connectAndParse("");
	}
	
	@Test(expectedExceptions = MalformedURLException.class)
	public void testConnectAndParse_InvalidUrl_malformed() throws IOException
	{
		objectUnderTest.connectAndParse(INVALID_URL);
	}
	
	@Test(expectedExceptions = MalformedURLException.class)
	public void testConnectAndParse_InvalidUrl_null() throws IOException
	{
		objectUnderTest.connectAndParse(null);
	}
}
