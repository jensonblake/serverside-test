package uk.nb.exception;

/**
 * 
 * @author Nathan Bradshaw
 *
 */
public class ParsingException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2831389763653789966L;
	
	public ParsingException(final String message)
	{
		super(message);
	}
}
