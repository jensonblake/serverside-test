package uk.nb;

import java.net.MalformedURLException;

/**
 * Main entry point
 *
 */
public class App 
{
	public static String URL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";
    public static void main( String[] args )
    {
    	final WebsiteParser parser = new WebsiteParser();   	
    	try
    	{
	    	final String json = parser.connectAndParse(URL);
	    	System.out.println(json);
    	}
    	catch(MalformedURLException e)
    	{
    		// Should never happen, but...
    		System.out.println("The provided URL: [" + URL + "] was malformed.");
    		System.out.println(e.getLocalizedMessage());
    	} 
    }
}
