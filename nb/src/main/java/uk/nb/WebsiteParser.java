package uk.nb;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import uk.nb.exception.ParsingException;
import uk.nb.io.PageFetcher;
import uk.nb.json.Item;
import uk.nb.json.OutputBuilder;

/**
 * Provides functionality to connect to the sample Sainsbury's website and parse some data
 * 
 * @author Nathan Bradshaw
 *
 */
public class WebsiteParser {
	
	private PageFetcher pageFetcher = new PageFetcher();
	private ItemParser itemParser = new ItemParser();
	private OutputBuilder outputBuilder = new OutputBuilder();
	
	/**
	 * Default Constructor
	 */
	public WebsiteParser() {}
	
	
	/**
	 * Connect to the website, parse the required data and return a json String of the required data
	 * 
	 * @return A String json representation of the data
	 * @throws MalformedURLException 
	 * @throws IOException 
	 */
	public String connectAndParse(final String mainUrl) throws ParsingException, MalformedURLException
	{
		validateUrl(mainUrl);
		
		// Get the item URLs to parse
		List<String> itemUrls = getItemUrls(mainUrl);
		
		// Get the item from each parsed item page
		List<Item> items = new ArrayList<>();
		itemUrls.forEach(itemUrl -> items.add(this.itemParser.parseItem(itemUrl)));
		
		// Build the output json
		return this.outputBuilder.buildJson(items);
	}
	
	
	private List<String> getItemUrls(final String mainUrl) throws ParsingException
	{
		List<String> itemUrls = new ArrayList<>();
		
		Document doc = this.pageFetcher.fetchDocument(mainUrl);
		Elements products = doc.getElementsByClass("productNameAndPromotions");
		
		// Collect and print some product URLs
		products.forEach(p -> p.getElementsByTag("a").forEach(a -> itemUrls.add(a.attr("href"))));
		
		return itemUrls;
	}
	
	
	private void validateUrl(final String url) throws MalformedURLException
	{
		// Cheap; use Javas limited URL validation
		new URL(url);
	}
}
