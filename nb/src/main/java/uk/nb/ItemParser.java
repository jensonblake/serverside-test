package uk.nb;


import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import uk.nb.exception.ParsingException;
import uk.nb.io.PageFetcher;
import uk.nb.json.Item;

/**
 * Provides functionality to parse each item page from the site
 * @author Nathan Bradshaw
 *
 */
public class ItemParser {
	
	private PageFetcher pageFetcher = new PageFetcher();
	private static String PREFIX  = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk";
	private static String SECTION = "/shop";
	
	/**
	 * Default Constructor
	 */
	public ItemParser() {}
	
	
	/**
	 * Parse the given URL and return the item details from that page
	 * @param url to parse
	 * @return {@link Item}
	 * @throws ParsingException 
	 */
	public Item parseItem(final String url) throws ParsingException
	{
		// Note - It looks like jsoup has a decent query ability - ideally I'd use this instead given time
		// See: https://jsoup.org/apidocs/org/jsoup/select/Selector.html
		
		Item item = new Item();
		Document subPage = pageFetcher.fetchDocument(processString(url));
		
		// Set the title
		item.setTitle(subPage.getElementsByClass("productTitleDescriptionContainer").get(0).getElementsByTag("h1").get(0).text());
		
		// Set the calories (if there)
		Elements possibleTables = subPage.getElementsByClass("nutritionTable");
		if (!possibleTables.isEmpty())
		{
			Element nutritionTable = possibleTables.get(0);
		
			// Assume a single nutrition table
			item.setKcal_per_100g(nutritionTable.getElementsByTag("tr").get(2).getElementsByTag("td").get(0).text());
		}
		
		// Set the price
		item.setUnit_price(subPage.getElementsByClass("pricePerUnit").get(0).text());
		
		// Set the Description
		item.setDescription(subPage.getElementsByClass("productText").get(0).getElementsByTag("p").get(0).text());
		
		return item;
	}
	
	
	private String processString(final String in)
	{
		int from = in.indexOf(SECTION);
		return PREFIX + in.substring(from);
	}
}
