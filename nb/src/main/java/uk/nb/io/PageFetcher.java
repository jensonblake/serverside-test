package uk.nb.io;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import uk.nb.exception.ParsingException;

/**
 * Simply wraps jsoup to make testing easier
 * @author Nathan Bradshaw
 *
 */
public class PageFetcher {
	
	/**
	 * Attempt to fetch the requested document
	 * @param url
	 * @return {@link Document}
	 * @throws ParsingException 
	 */
	public Document fetchDocument(final String url) throws ParsingException
	{
		try
		{
			return Jsoup.connect(url).get();
		}
		catch(IOException e)
		{
			throw new ParsingException(e.getMessage());
		}
	}
}
