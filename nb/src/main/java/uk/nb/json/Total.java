package uk.nb.json;

/**
 * Defines the 'total' object for outputting price attibutes
 * @author Nathan Bradshaw
 *
 */
public class Total {
	
	private double gross;
	private double vat;
	
	/**
	 * Default Constructor
	 */
	public Total(){}
	
	public double getGross() {
		return gross;
	}
	public void setGross(double gross) {
		this.gross = gross;
	}
	public double getVat() {
		return vat;
	}
	public void setVat(double vat) {
		this.vat = vat;
	}
}
