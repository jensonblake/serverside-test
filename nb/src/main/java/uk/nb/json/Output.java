package uk.nb.json;

/**
 * Defines the total output of this scrape
 * @author Nathan Bradshaw
 *
 */
public class Output {
	
	/**
	 * Default Constructor
	 */
	public Output(){}
	
	private Item[] results;
	private Total total;
	
	public Item[] getResults() {
		return results;
	}
	
	public void setResults(Item[] results) {
		this.results = results;
	}
	
	public Total getTotal() {
		return total;
	}
	
	public void setTotal(Total total) {
		this.total = total;
	}
}
