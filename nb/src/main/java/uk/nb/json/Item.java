package uk.nb.json;

/**
 * Defines a berry, cherry or currant item
 * 
 * @author Nathan Bradshaw
 *
 */
public class Item {
	
	/**
	 * Default constructor
	 */
	public Item(){}
	
	private String title;
	private Integer kcal_per_100g;
	private double unit_price;
	private String description;
	
	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Integer getKcal_per_100g() {
		return kcal_per_100g;
	}


	public double getUnit_price() {
		return unit_price;
	}


	public void setUnit_price(final String price) {
		String numericPrice = price.replaceAll("[^\\d.]", "");
		this.unit_price = Double.valueOf(numericPrice);
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setKcal_per_100g(String kcal) {
		String numeric = kcal;
		if (kcal.contains("kcal"))
		{
			numeric = kcal.substring(0, kcal.lastIndexOf("kcal"));
		}
		this.kcal_per_100g = Integer.valueOf(numeric);
	}
}
