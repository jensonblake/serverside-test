package uk.nb.json;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;

/**
 * Provides functionality to build json output from the given set of items
 * @author Nathan Bradshaw
 *
 */
public class OutputBuilder {
	
	private Gson parser;
	
	/**
	 * Default Constructor
	 */
	public OutputBuilder() 
	{	
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting();
		builder.disableHtmlEscaping();
		builder.registerTypeAdapter(Double.class, (JsonSerializer<Double>) (src, typeOfSrc, context) -> {
		    DecimalFormat df = new DecimalFormat("#.##");
		    df.setRoundingMode(RoundingMode.CEILING);
		    return new JsonPrimitive(Double.parseDouble(df.format(src)));
		});
		parser = builder.create();
	}
	
	
	public String buildJson(final List<Item> items)
	{
		// Set the items
		Output output = new Output();
		output.setResults(items.toArray(new Item[] {}));
		
		// Calculate and set the totals
		Total total = new Total();
		double gross = 0.0;
		for (Item i : items)
		{
			gross += i.getUnit_price();
		}
		double vat = gross/100*20;
		total.setGross(gross);
		total.setVat(vat);
		output.setTotal(total);
		
		// Return the json representation of this object
		return parser.toJson(output);
	}
}
